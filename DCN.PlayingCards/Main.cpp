
// Playing Cards  - Lab Excersise 2
// Devon Norman
// Ryan

#include <iostream>
#include <conio.h>

using namespace std;

//Struct for Card object.
struct Card
{
	Rank Rank;
	Suit Suit;
};

//Enum for card ranks.
enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack, // Eleven
	Queen, // Twelve
	King, // Thirteen
	Ace // Fourteen
};

// enum for card suits.
enum Suit
{
	Heart,
	Diamond,
	Clubs,
	Spades
};



int main()
{


	(void)_getch();
	return 0;
}
